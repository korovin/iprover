#!/bin/bash

#set -x

# TODO: add script options; use other 
#  override_cmd  override_cmd_opts  add_cmd_options  override_problem_path bench.csv list

FILE=tests_basic.csv
CMD_PATH=.

PASSED=0
FAILED=0

glb_start_time=`date +%s`

echo 
echo "======= Benchmarks: $FILE =========="
echo
echo "test_id cmd opts problem timeout exp_result descr"

while IFS="," read -r test_id cmd opts problem timeout exp_result descr
do
    echo "======= TEST $test_id"    
    echo "$test_id, $cmd, $opts, $problem, $timeout, $exp_result, $descr"
    
    lcl_start_time=`date +%s`
    echo "$cmd $opts $problem"
    $cmd $opts $problem 2>&1 | grep -q "$exp_result"  > /dev/null 2>&1
    lcl_end_time=`date +%s`
    lcl_runtime=$((lcl_end_time - lcl_start_time))
    
    passed=$?
    if [ $passed -eq 0 ]; then
        echo "PASSED: ${lcl_runtime}s"
        ((PASSED++))
    else
        echo "FAILED: ${lcl_runtime}s"
        ((FAILED++))
    fi
done < <(tail -n +2 $FILE)

glb_end_time=`date +%s`

glb_runtime=$((glb_end_time - glb_start_time))

echo
echo "======= TOTAL: ${glb_runtime}s"
echo "PASSED: $PASSED"
echo "FAILED: $FAILED"



exit $FAILED
