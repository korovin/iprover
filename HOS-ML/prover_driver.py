import atexit
import subprocess
import sys
import os
import time
import signal

import prover_runner as pr
import config
from preprocessing import handle_pre_processing, clausify_smt_problem
import ltb_module
import helper as hp
from helper import ProblemVersion
from parser import get_parser, check_illegal_args


# Setup root logger
import logging

logging.basicConfig(stream=sys.stdout, level=logging.WARNING, format="%(levelname)s - %(message)s")
log = logging.getLogger()


def main(
    problem_path,
    wc_timeout,
    schedule="default",
    schedule_mode="external",
    prover=None,
    pre_processor_heuristic=None,
    pre_processor_timeout=200,
    no_cores=8,
    problem_version=None,
    ltb=False,
    ltb_problem=False,
    ltb_version="+1",
    ltb_schedule_low_v4=None,
    ltb_schedule_low_v5=None,
    ltb_schedule_high=None,
    ltb_output_dir=".",
    suppress_proof_model_out=False,
    heuristic_context="default",
    loglevel=logging.WARNING,
    run_clausification_test=False,
):

    # Set the loglevel
    log.setLevel(loglevel)
    # Set the global proof reconstruction flag
    config.PROOF_MODEL_OUT = not suppress_proof_model_out
    # Set the context
    config.HEURISTIC_CONTEXT = heuristic_context

    # Check that problem/batchfile exists exists and exit if it doesn´t
    hp.check_file_existence(problem_path)

    # Set the prover path
    set_prover(prover)

    # Get the current time as start time
    start_time = time.time()

    # Add  grace
    time_left = wc_timeout + config.GRACE
    log.info("Added {0}s grace, new timelimit is: {1}".format(config.GRACE, time_left))

    # Get the problem version
    # FIXME maybe make separate function?
    if problem_version is None:
        # Infer the problem version from the given problem
        config.PROBLEM_VERSION = hp.infer_problem_version(problem_path)
    else:
        # Convert the string to the enum
        config.PROBLEM_VERSION = hp.get_problem_version_from_string(args.problem_version)
    log.info(f"Problem version is set to: {config.PROBLEM_VERSION}")

    # TODO unclear whether this is the best solution - quite messy
    # If the problem is SMT but not using UF logic, we need to treat it as an LTB problem
    if config.PROBLEM_VERSION == hp.ProblemVersion.SMT2:
        if not hp.smt_problem_uses_uf_logic(problem_path):
            ltb_problem = True
            log.info("SMT problem not using UF logic. Treating SZS statuses as LTB.")

    # SMT problems has to be preprocessed separately
    if config.PROBLEM_VERSION == hp.ProblemVersion.SMT2:
        log.info("Clausifying SMT problem")
        problem_path = clausify_smt_problem(problem_path, wc_timeout)

        # Prep did not solve the problem, update the time left and return the prep problem
        time_left = time_left - (time.time() - start_time)
        log.info("Time spent on clausifying: {0:.2f}".format(time.time() - start_time))
        log.info("Time left for prover: {0:.2f}".format(time_left))

    # Pre process problem if pre_processor is set (not avaliable for LTB batch mode)
    if pre_processor_heuristic and not ltb:
        log.debug("Preprocessing problem")
        problem_path = handle_pre_processing(
            problem_path, pre_processor_heuristic, pre_processor_timeout, time_left, ltb
        )

        # Prep did not solve the problem, update the time left and return the prep problem
        time_left = time_left - (time.time() - start_time)
        log.info("Time spent on prep: {0:.2f}".format(time.time() - start_time))
        log.info("Time left for prover: {0:.2f}".format(time_left))

    # Either run the LTB or standard setup
    if ltb:
        log.debug("Running LTB setup")
        handle_ltb_run_new(problem_path, ltb_output_dir, schedule_mode, no_cores, schedule, ltb_version)

    else:
        log.debug("Running standard setup")

        handle_prover_run(
            problem_path,
            start_time,
            time_left,
            schedule,
            schedule_mode,
            no_cores,
            ltb_problem,
            run_clausification_test,
        )


def set_prover(prover):
    """
    Update the prover path if set.
    """
    if prover is not None:
        # Update path
        config.PROVER = os.path.expanduser(prover)
        # Default prover is not used anymore
        config.DEFAULT_PROVER = False
        log.debug(f"Prover path is updated to: {config.PROVER}")


# TODO after the clausification test, we should return this as the new problem!
def parsing_test_succesfull(problem, timeout):

    # Check if TPTP variable is correctly set for the includes
    try:
        os.environ["TPTP"]
    except KeyError:
        log.error("TPTP variable not set correctly for clausification test")
        return False

    try:
        # Start the clausification test
        # tclausify or clausify???

        cmd = f"{config.CLAUSIFIER} --mode {hp.get_clausifier_mode()} -t {timeout} --include $(echo $TPTP) {problem}"

        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        log.debug("Clausify testing: {0}".format(cmd))

        # Wait for embedding to finish
        outs, errs = proc.communicate(timeout=max(8, timeout + 2))

        # TODO this should be moved somewhere else!
        # If the problem version currently is TF0, we check the output of the clausification to see whether
        # it should instead be a TX0 problem
        if config.PROBLEM_VERSION == ProblemVersion.TF0:
            # Check if TX0 signature is in the clausified problem
            if b"X0 : $o" in outs:
                # Set problem version to TX0
                config.PROBLEM_VERSION = ProblemVersion.TX0
                log.info(f"TX0 detected. Changed problem version to: {config.PROBLEM_VERSION}")

    except subprocess.TimeoutExpired:
        # If communicate timed out the test clairly failed
        log.warning("Clausify test subprocess.TimeoutExpired")
        proc.kill()
        outs, errs = proc.communicate()
        return False

    test_passed = proc.returncode == 0 and errs == b""
    log.info("Clausification test passed: {0}".format(test_passed))
    if not test_passed:
        log.debug(f"Clausifier exitcode: {proc.returncode}")
        log.debug(f"Clausifier error msg: {errs}")

    return test_passed


# Handler for standard prover runs (non-LTB)
def handle_prover_run(
    problem, start_time, time_left, schedule, schedule_mode, no_cores, ltb_problem, run_clausification_test
):

    # Start proving
    print("% SZS status Started for {0}".format(problem))

    # Check if we can parse the problem in one second, otherwise, change the schedule mode to internal
    # Check if internal scheduling is more beneficial if problem takes time to parse
    # But do not do this if SiNE is selected.
    if (
        run_clausification_test and not schedule_mode != "internal" and not ltb_problem
    ):  # TODO this should not be ran if preprocessing heuristic is set
        if not parsing_test_succesfull(
            problem, config.PARSE_TEST_TIMEOUT
        ):  # FIXME this fails too often for silly reasons
            schedule_mode = "internal"
            log.info("Changed schedule mode to internal")
        # Update time left
        time_left = time_left - (time.time() - start_time)
        log.info("Time left: {0:g}".format(time_left))

    # Run the prover setup
    success, output_file = run_prover_setup(
        problem, time_left, schedule, schedule_mode, no_cores, ltb_mode=ltb_problem
    )

    if success:
        # Print the proof
        hp.output_proof(output_file)
        # Proof has been outputted by the above. Unregister any proof other proof outputstart
        atexit.unregister(hp.output_proof)

    else:
        print("% SZS status Unknown for {0}".format(problem))


# TODO could use the enum version for this as well??
def handle_ltb_run_new(batchfile_path, proof_output_dir, schedule_mode, no_cores, schedule, ltb_version):

    # Deduce whether the problems to attempt are tff
    # TODO this is not longer how we do this. Need to think of something new ..
    tff_mode = ltb_version == "_1"
    log.debug("tff_mode is set to: {0}".format(tff_mode))

    # Make the output directory
    if not os.path.exists(proof_output_dir):
        os.makedirs(proof_output_dir)
        log.info("Created proof output dir: {0}".format(proof_output_dir))

    # Get the start time
    start_time = time.time()

    # Get attributes of the batchfile
    batch_dir, time_limit, problems = ltb_module.process_batchfile(batchfile_path)

    # Create batch manager
    ltb_batch = ltb_module.create_ltb_batch(batch_dir, time_limit, problems)
    log.info("Loaded and created LTB batch: {0}".format(ltb_batch))

    # Start the one and only attempt on the problems as there is only one version
    time_left = time_limit - (time.time() - start_time)
    log.info("Starting schedule on batch for time: {0:.2f}".format(time_left))
    log.info(f"The minimum time limit per problems is set to: {config.LTB_MIN_PROBLEM_TIMELIMIT}")
    run_ltb_problem_attempt(
        ltb_batch,
        proof_output_dir,
        schedule_mode,
        schedule,
        time_left,
        no_cores,
        config.LTB_MIN_PROBLEM_TIMELIMIT,
        version=ltb_version,
    )

    """
    # Attempt the unsolved problems a second time in case we have more time left (due to last problems being solved)
    time_left = time_limit - (time.time() - start_time)
    if time_left > 0:
        log.info("Rerunning schedule on batch for time: {0:.2f}".format(time_left))
        # Run on FOF problems, with no extra time per problem
        run_ltb_problem_attempt(
            ltb_batch,
            proof_output_dir,
            schedule_mode,
            schedule,
            time_left,
            no_cores,
            config.LTB_MIN_PROBLEM_TIMELIMIT,
            False,
            version="+1",
        )
    else:
        log.info("Not enough time to start the second attempt")
    """

    log.info("Finished batch attempt")


def run_ltb_problem_attempt(
    ltb_batch,
    proof_output_dir,
    schedule_mode,
    schedule,
    wc_time,
    no_cores,
    min_problem_timelimit,
    version=None,
):

    # Meta of the attempt
    attempt_start = time.time()
    problems_in_attempt = ltb_batch.get_no_unsolved_problems()

    for problem_attempt_no, problem in enumerate(list(ltb_batch.get_unsolved_problems())):

        # Variable for logging time taken
        prob_start = time.time()
        time_left = wc_time - (time.time() - attempt_start)

        # Log time variables of this attempt
        log.debug("time_left : {:.2f}".format(time_left))
        log.debug("total_time_used : {:.2f}".format(time.time() - attempt_start))
        log.debug("problems left: {}".format((problems_in_attempt - problem_attempt_no)))

        # Compute the time available for this problem attempt!
        attempt_timelimit = min(
            time_left + 1, max(time_left / (problems_in_attempt - problem_attempt_no), min_problem_timelimit)
        )
        log.debug("attempt_timelimit : {:.2f}".format(attempt_timelimit))

        if attempt_timelimit < 0:
            log.info("Not enough time to continue the batch attempt. Exiting..")
            return ltb_batch

        # Get the problem_path from the problem
        problem_path, version_name = problem.get_path(version=version)
        print("% SZS status Started for {0}".format(version_name))

        # Attempt the problem with the schedule
        success, output_file = run_prover_setup(
            problem_path, attempt_timelimit, schedule, schedule_mode, no_cores, ltb_mode=True
        )

        if success:
            print("% SZS status Theorem for {0}".format(version_name))
            ltb_batch = ltb_module.handle_solved_problem(ltb_batch, proof_output_dir, problem, output_file)
        else:
            print("% SZS status GaveUp for {0}".format(version_name))

        print("% SZS status Ended for {0}".format(version_name))
        log.debug("Problem attempt used time: {:.2f}".format(time.time() - prob_start))

        # Empty prover processes for this problem attempt
        hp.kill_all_prover_processes()

        # Clean up tmp files between each problem
        hp.clean_tmp_folder_contents()

    return ltb_batch


def get_schedule_conf(schedule, schedule_mode, problem_path, no_cores):

    try:
        schedule_conf = config.SCHEDULES[schedule]
        log.info("Obtained schedule: {0}".format(schedule))
    except KeyError:
        log.error('The schedule "{0}" is not implemented.'.format(schedule))
        sys.exit(1)

    if isinstance(schedule_conf, dict):
        # The schedule is admissible, so find the local schedule
        log.info("Admissible schedules, predicting local schedule")
        local_schedule = predict_local_schedule(schedule_conf, problem_path, no_cores)
        log.info("Obtained local schedule: {0}".format(local_schedule))
        # Extract the appropriate schedule
        schedule_conf = schedule_conf[local_schedule]

    # If we use multiple cores (nested lists) in external mode, we need to flatten the schedule
    if isinstance(schedule_conf, list) and isinstance(schedule_conf[0], list) and schedule_mode == "external":
        schedule_conf = get_core_order(schedule_conf)

    # Handle internal prep scheduling if set
    if schedule_mode == "internal":
        # Get the prep heuristic
        prep_heuristic = get_scheule_internal_prep_heuristic(schedule)
        log.info("Using prep heuristic: {0}".format(prep_heuristic))

        # Transform schedule into single core if not in core format
        if isinstance(schedule_conf, list) and isinstance(schedule_conf[0], tuple):
            log.info("Placing internal schedule on a single core")
            schedule_conf = [schedule_conf]
    else:
        prep_heuristic = None

    log.info("Final schedule: {0}".format(schedule_conf))

    return schedule_conf, prep_heuristic


def get_core_order(schedule):

    start_times = {}
    run_times = {}
    for core in schedule:
        # The start time on a core is always zero
        current_time = 0
        for heur, run_time in core:
            # Extract the runtime
            run_times[heur] = run_time
            # Set the start time of theu heuristic
            start_times[heur] = current_time
            # Increment the time
            current_time += run_time

    # Sort heuristics according to their start times
    heur_order = [k for k, _ in sorted(start_times.items(), key=lambda item: item[1])]

    # Rebuild flat schedule
    flattened = []
    for heur in heur_order:
        flattened += [(heur, run_times[heur])]

    return flattened


def embed_problem(schedule, problem, no_cores, print_proof=True):

    # Run the problem mebedding
    cmd = "./dist/run_embedding/run_embedding {0} {1} {2} --clausifier_path {3} --no_cores {4}".format(
        config.PROVER, schedule["model"], problem, config.CLAUSIFIER, no_cores
    )
    proc = subprocess.Popen(
        cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid
    )
    log.debug("Getting embedding: {0}".format(cmd))
    try:
        # Wait for embedding to finish
        outs, errs = proc.communicate(timeout=15)

        # If there was an error while embedding the problem, return None
        if not (proc.returncode == 0 or proc.returncode == 10) or errs != b"":
            log.warning("Embedding code: {0} msg: {1}".format(proc.returncode, errs))
            return None

    except subprocess.TimeoutExpired:
        log.warning("run_embedding subprocess.TimeoutExpired")
        os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
        outs, errs = proc.communicate()
        return None

    # If the problem was solved, report and quit!
    if proc.returncode == 10:
        log.info("Problem was solved while embedding")
        if print_proof:
            log.info("Printing proof and exiting")
            print(outs.decode("utf-8"))
            sys.exit(0)
        else:
            # For use with the simulation code
            return "solved"

    # convert string output to list of floats and return the embedding vector
    embedding = [float(s) for s in outs.decode("utf-8")[:-1].strip("][").split()]
    log.debug("Obtained embedding: {0}".format(embedding))
    return embedding


def map_embedding_to_cluster(embedding, cluster_centers):

    # Get the embedding and map to the local schedule
    try:
        cluster = hp.get_closest_cluster_center(embedding, cluster_centers)
        return cluster
    except Exception as err:
        log.warning("Unexpected exception during schedule mapping: {0}".format(err))
        # Return default scheudle if something goes wrong
        return "default"


def predict_local_schedule(schedule, problem, no_cores, print_proof=True):
    # Obtain the problem embedding
    embedding = embed_problem(schedule, problem, no_cores, print_proof=print_proof)

    # If embedding was unsuccessfull, return fallback
    if embedding is None:
        return "default"
    elif embedding == "solved":  # Used with the simulation code
        return "solved"

    cluster = map_embedding_to_cluster(embedding, schedule["cluster_centers"])
    if cluster is None:
        return "default"
    else:
        return cluster


def get_scheule_internal_prep_heuristic(schedule):

    # Get the schedule preprocessor
    try:
        internal_prep_heuristic = config.INTERNAL_PREP_HEURISTICS[schedule]
    except KeyError:
        log.warning('The prep heuristic for "{0}" is not implemented. Using default prep. '.format(schedule))
        internal_prep_heuristic = "heur/schedule_none"

    return internal_prep_heuristic


def run_prover_setup(problem_path, time_left, schedule, schedule_mode, no_cores, ltb_mode=False):

    # Keep track of time
    now = time.time()

    # Compute the schedule setup and run
    log.debug("Computing schedule configuration")
    schedule_conf, prep_heuristic = get_schedule_conf(schedule, schedule_mode, problem_path, no_cores)
    # Get time left after finding the schedule
    time_left = time_left - (time.time() - now)
    log.info("Time left after finding schedule: {0:g}".format(time_left))

    log.debug("Running schedule in mode: {0}".format(schedule_mode))
    success, proof_file = pr.run_schedule(
        problem_path, schedule_mode, schedule_conf, time_left, no_cores, ltb_mode, prep_heuristic
    )

    return success, proof_file


if __name__ == "__main__":

    # Get the arguments
    parser = get_parser()

    # Parse the arguments
    args = parser.parse_args()

    # Report id debug
    if args.loglevel == logging.DEBUG:
        print(args)

    # Check for illegall arg combinations: This can probably be done smarter
    # in the argparses (later)
    check_illegal_args(args)

    # Register system signals for the script
    hp.register_signals()

    # Run the program
    main(**vars(args))
    log.info("Finished")
