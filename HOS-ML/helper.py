import shutil
import os
import sys
import re
import atexit
import config
import tempfile
import signal
import glob
from enum import Enum, auto

# Get the root logger
import logging

log = logging.getLogger()

# TODO: all this should be put in a utils module


class ProblemVersion(Enum):
    CNF = auto()
    FOF = auto()
    TF0 = auto()
    TX0 = auto()
    SMT2 = auto()


def get_clausifier_mode() -> str:

    # Check if not set, or not set properly
    if config.PROBLEM_VERSION is None or not isinstance(config.PROBLEM_VERSION, ProblemVersion):
        log.error(
            f"Trying to get clausifier mode before setting the problemVersion: {config.PROBLEM_VERSION}"
        )
        return "clausify"
    # TFF version of the problem
    elif (
        config.PROBLEM_VERSION == ProblemVersion.TF0
        or config.PROBLEM_VERSION == ProblemVersion.TX0
        or config.PROBLEM_VERSION == ProblemVersion.SMT2
    ):
        return "tclausify"
    else:
        return "clausify"


def get_problem_version_from_string(version):
    """Converts the string version value into the ProblemVersion"""

    if version == "cnf":
        return ProblemVersion.CNF
    elif version == "fof":
        return ProblemVersion.FOF
    elif version == "tff":
        return ProblemVersion.TF0
    elif version == "smt":
        return ProblemVersion.SMT2
    elif version == "tx0":
        return ProblemVersion.TX0
    else:
        raise ValueError(f"ProblemVersion {version} is not supported for the driver script.")


def read_problem(problem_path):
    with open(problem_path, "r") as f:
        problem = f.read()
    return problem


def infer_problem_version(problem_path):
    """Function that tries to infer the problem type based on its contents."""

    # Get the contents of the problem
    problem = read_problem(problem_path)

    if re.search(r"cnf\(", problem):
        return ProblemVersion.CNF
    elif re.search(r"fof\(", problem):
        return ProblemVersion.FOF
    elif re.search(r"tff\(", problem):
        return ProblemVersion.TF0
    elif re.search(r"\(set-logic|\(declare-sort|\(declare-fun", problem):
        return ProblemVersion.SMT2
    else:
        # Log a warning message
        log.warning("Could not infer the problem version. Setting FOF as default")
        return ProblemVersion.FOF


def smt_problem_uses_uf_logic(problem_path) -> bool:
    """function that checks whether the problem uses UF logic"""

    # Get the contents of the problem
    problem = read_problem(problem_path)

    if re.search(r"\(set-logic UF\)", problem):
        return True
    else:
        return False


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def get_tmp_out_file():
    # Create the tmp file in the current tmp directory and return the file name
    fd, filepath = tempfile.mkstemp(prefix=config.TMP_DIR + "/")
    os.close(fd)  # Close the open file descriptor
    return filepath


# TODO I want this to be conditional when in a DEBUG mode
@atexit.register
def clean_tmp_folder():
    # Clean tmp folder
    try:
        shutil.rmtree(config.TMP_DIR)
        log.debug('Removed tmp dir: "{0}"'.format(config.TMP_DIR))
    except FileNotFoundError:
        pass


def clean_tmp_folder_contents():

    try:
        files = glob.glob(config.TMP_DIR + "/*")
        for f in files:
            os.remove(f)
    except Exception as err:
        log.warning("Could not rremove some file within the tmp dir: {0}".format(err))


def graceful_exit(signal_number, frame):
    log.info("Recieved signal {0}, killing processes and removing temp files".format(signal_number))
    # Kill all processes
    kill_all_prover_processes()
    # Cleanup all folders
    clean_tmp_folder()
    sys.exit(0)


def register_signals():

    # Register the signals
    log.debug("Registering signals SIGINT and SIGTERM")
    signal.signal(signal.SIGINT, graceful_exit)
    signal.signal(signal.SIGTERM, graceful_exit)  # TODO: SIGTERM does not catch from bash script


# Function for checking which processes in a process dictionary that is still running
# and terminated that process group (parent and children)
def kill_prover_process(prover_proc):

    # If process is still active
    if prover_proc.proc.poll() is None:
        # Get process group id and kill the group
        pgrp = os.getpgid(prover_proc.proc.pid)
        os.killpg(pgrp, signal.SIGKILL)
        log.debug("Killed process: {0}".format(pgrp))


# Kills running processes and updates the CPU times for cleaning up
@atexit.register
def kill_all_prover_processes():
    log.debug("Start killing of all prover processes")

    # Kill each process
    for process_key in list(config.PROVER_PROCESSES.keys()):
        # Kill the process
        kill_prover_process(config.PROVER_PROCESSES[process_key])

        # Delete the tmp output file
        try:
            os.remove(config.PROVER_PROCESSES[process_key].output_file)
        except FileNotFoundError:
            pass
        # Remove from process dict
        del config.PROVER_PROCESSES[process_key]

    log.debug("Killed all prover processes")


def check_file_existence(file_path):

    # If a file doesn´t exists, exit on error
    if not os.path.exists(file_path):
        eprint('ERROR: file "{0}" does not exist'.format(file_path))
        sys.exit(1)


def output_proof(proof_file):

    with open(proof_file, "r", newline=None) as f:
        proof = f.read()

    log.debug("Outputitng proof from {0}".format(proof_file))
    print(proof)


def compute_timeout_bound(process_timeout, wc_timeout):

    if process_timeout is None:
        timeout = wc_timeout
    else:
        timeout = min(process_timeout, wc_timeout)
    log.debug("Timeout bound: {0}".format(timeout))

    return timeout


# Make test cases first!
# Count the number of clauses in a problem (including its axioms)
def count_no_clauses(problem_path):

    # Variable to store the number of fof clauses
    count = 0

    # Set in a try as we might not find the axiom file
    # (bit dependent on the file structure)
    try:
        # First open up the problem and count the clauses
        with open(problem_path, "r") as f:
            data = f.read()

        # Get all the clauses in the problem
        count += len(re.findall("^fof", data, flags=re.MULTILINE))

        # See if there are any axiom files
        axioms = re.findall("^include.*", data, flags=re.MULTILINE)

        for axiom in axioms:
            # Get the axiom file
            ax_file = axiom[9:-3]

            # Get the path (a bit silly)
            dir_path = os.path.dirname(problem_path)
            dir_path = os.path.dirname(dir_path)
            ax_path = dir_path + "/" + ax_file

            # First open up the problem and count the clauses
            with open(ax_path, "r") as f:
                data = f.read()

            # Get all the clauses in the problem
            count += len(re.findall("^fof", data, flags=re.MULTILINE))
    except Exception:
        count = 0

    log.info("{0} clauses in {1}".format(count, problem_path))
    return count


def common_solved(u, v, **kwargs):

    # Only import if called specifically
    import numpy as np

    u = np.asarray(u, dtype=np.float32)
    v = np.asarray(v, dtype=np.float32)

    if len(u.shape) == 1:
        u = np.asarray([u], dtype=np.float32)

    if len(v.shape) == 1:
        v = np.asarray([v], dtype=np.float32)

    # Solves both
    common = np.sum(np.multiply(u, v), axis=1) * 2

    # Compute number of solved
    solved = np.sum(u, axis=1) + np.sum(v, axis=1)

    # Compute the ratio
    with np.errstate(divide="ignore", invalid="ignore"):
        ratio = np.nan_to_num(common / solved, nan=0.0)
    dist = 1 - ratio

    return dist


# Native version of the function above, given one dimensional vectors
def common_solved_native(u, v):

    # Compute number of problems solved in both
    common = 0
    for a, b in zip(u, v):
        common += a * b

    # Double
    common *= 2

    # Compute total number solved
    solved = sum(u) + sum(v)

    # Compute ratio
    try:
        ratio = common / solved
    except ZeroDivisionError:
        ratio = 0

    # Compute the distance
    distance = 1 - ratio
    return distance


def get_closest_cluster_center(vector, cluster_centers):

    # Compute the distance to each center
    distances = []
    for n, center in enumerate(cluster_centers):
        distance = common_solved_native(vector, center)
        distances += [distance]

    # Compute argmin
    closest_index = min(range(len(distances)), key=lambda x: distances[x])
    return closest_index
