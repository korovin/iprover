import shutil
import os
import re

import logging

log = logging.getLogger()


class LTBBatch(object):
    def __init__(self, batch_dir, time_limit):
        self.batch_dir = batch_dir
        self.time_limit = time_limit

        # Lists for keeping track of the solved/unsolved problems
        self.solved = {}
        self.unsolved = {}
        self.no_problems = 0

    def load_problem(self, loc_path, name):

        # Create a problem object
        ltb_problem = LTBProblem(loc_path, name)

        # Check if exists
        if not ltb_problem.exists():
            log.error('LTB Problem "{0}" with loc path "{1}" does not exists'.format(name, loc_path))

        # Add to list of unsolved problems
        self.unsolved[name] = ltb_problem

        # Update number of problems in the batch
        self.no_problems += 1

    def get_no_unsolved_problems(self):
        return len(self.unsolved)

    def get_unsolved_problems(self):
        return self.unsolved.values()

    def get_no_solved_problems(self):
        return len(self.solved)

    def get_solved_problems(self):
        return self.solved.values()

    def get_problem(self, name):
        return self.unsolved.get(name)

    def get_batch_size(self):
        return self.no_problems

    def set_problem_solved(self, prob):
        # Either supply the problem object, or the name
        if isinstance(prob, LTBProblem):
            prob_name = prob.name
        else:
            prob_name = prob

        try:
            # Pop the problem off the solved queue
            ltb_problem = self.unsolved.pop(prob_name)

            # Add it to the solved queue
            self.solved[prob_name] = ltb_problem
        except KeyError:
            log.error('Cannot set problem "{0}" as solved, as not in the unsolved queue.'.format(prob_name))

    def __str__(self):
        return "LTBBatch -> batch_dir: {0} timelimit: {1} no_solved: {2} no_unsolved: {3}".format(
            self.batch_dir, self.time_limit, len(self.solved), len(self.unsolved)
        )


class LTBProblem(object):
    def __init__(self, path, name):
        self.path = path
        self.name = name
        # self.versions = ["+1", "_1"]
        self.versions = ["_1"]  # FIXME only using _1 this year

    def exists(self):
        # Check if the problem actually exists on all version
        for v in self.versions:
            # Create the full version path
            problem = self.path + self.name + v + ".p"
            if not os.path.exists(problem):
                return False

        # All versions exists, return True
        return True

    def get_path(self, version=None):
        if version is None or version == "+1":
            return self.path + self.name + "+1.p", self.name + "+1"
        elif version is None or version == "_1":
            return self.path + self.name + "_1.p", self.name + "_1"
        elif version == "+5":
            return self.path + self.name + "+5.p", self.name + "+5"
        elif version == "+4":
            return self.path + self.name + "+4.p", self.name + "+4"
        else:
            # Not recognised the version. Report and return version +1
            log.error("Version {0} not recognised. Returning version +1".format(version))
            return self.path + self.name + "+1.p", self.name + "+1"

    def __str__(self):
        return "LTBProblem -> Name: {0} Path: {1} Versions: {2} Exists: {3}".format(
            self.name, self.path, ",".join(self.versions), self.exists()
        )


def create_ltb_batch(batch_dir, time_limit, problems):

    # Initialise object
    ltb_batch = LTBBatch(batch_dir, time_limit)

    # Load the problems
    for subdir, name in problems:
        # Create full path
        loc_path = batch_dir + "/" + subdir + "/"
        ltb_batch.load_problem(loc_path, name)

    log.info(f"Number of problems loaded from batch: {ltb_batch.get_batch_size()}")

    return ltb_batch


def process_batchfile(batchfile_path):

    # This function implements a few shortcuts according to the J10 instruction
    # - Problems are unordered
    # - output.required Proof
    # - limit.time.problem.wc 0  # No problem timelimit

    # Get the directory path where it is all happending
    batch_dir = os.path.dirname(os.path.realpath(batchfile_path))

    # List for holding the problems in the batch
    problems = []

    # Whether problem listings have started
    read_problem = False

    # Get contents of the batch file
    with open(batchfile_path, "r") as bf:
        batch_spec = bf.read().splitlines()

    # Process the batchfile
    for line in batch_spec:
        # Obtain time limit
        if re.match("limit.time.overall.wc", line):
            time_limit = int(line.split()[1])
        # Set starting to read batch
        if line == "% SZS start BatchProblems":
            read_problem = True
        # Set stopping to read batch
        elif line == "% SZS end BatchProblems":
            read_problem = False
        elif read_problem:
            # Get sub directory path and problem name
            prob = line.split()  # Get words
            problems += [["/".join(prob[0].split("/")[:-1]), prob[1]]]

    return batch_dir, time_limit, problems


def output_proof_to_file(output_dir, name, proof_file):

    # Still wsant nto continue proving!
    try:
        proof_file_dest = output_dir + "/" + name
        shutil.copyfile(proof_file, proof_file_dest)
        log.debug("Outputting proof of {0} to {1}".format(name, proof_file_dest))
    except Exception as err:
        log.error(
            "Could not copy proof file {0} to {1} due to: {2}".format(proof_file, proof_file_dest, str(err))
        )


def handle_solved_problem(ltb_batch, proof_output_dir, problem, proof_file):

    # Print to file
    output_proof_to_file(proof_output_dir, problem.name, proof_file)

    # Set problems as solved in the batch
    ltb_batch.set_problem_solved(problem)

    # Return the new batch
    return ltb_batch
