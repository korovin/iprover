#from iprover_process_module import start_prover_process, ProbProc, get_output_status, check_prover_success
#from scheduler import read_heuristic_string
import os
#from prover_runner import start_prover_external_schedule, get_prover_output_status, start_prover_internal_schedule
from prover_runner import build_prover_cmd_external, start_prover_process, get_prover_output_status
from config import SCHEDULES
import subprocess
from prover_driver import predict_local_schedule

# Check that every heuristic (and the caller) runs bug free on the prover

TIME = 3
test_problem = "test_problems/problem_fof.p"

import config
import helper
config.PROBLEM_VERSION = helper.ProblemVersion.FOF
#test_problem = "test_problems/floor256.qdimacs"
#test_problem = "test_problems/GRP564-1.p"
#test_problem = "test_problems/COL004-3.p"


########################################################
# Check configs


# Extract the heuristic configs
def extract_all_heuristic_configs():
    # Store all heuristic-time pairs
    heur_conf = []
    for name, sched in SCHEDULES.items():
        # If it is a single schedule
        if isinstance(sched, list) and isinstance(sched[0], tuple):
            for h in sched:
                heur_conf += [h]
        # Multi core schedule
        elif isinstance(sched, list) and isinstance(sched[0], list):
            for core in sched:
                for h in core:
                    heur_conf += [h]
        elif isinstance(sched, dict):
            for k, v in sched.items():
                # Check if default or a cluster representation
                if k == "default" or isinstance(k, int):
                    if isinstance(v, list) and isinstance(v[0], list):
                        for core in v:
                            for h in core:
                                heur_conf += [h]
                    else:
                        for h in v:
                            heur_conf += [h]

            # Check that model exists
            if not os.path.isfile(sched["model"]):
                print("ERROR: Model path {0} not found for {1}".format(sched["model"], name))
            # Check that there is a default scheudle
            if "default" not in sched:
                print("ERROR: Schedule {0} has no default schedule".format(name))
        else:
            print("Uncrecognised format for: ", name)

    return heur_conf


def check_conf_exists(heur_conf):

    # Check that every referenced heuristic exists
    to_download = []
    for conf in heur_conf:
        if not os.path.isfile(conf[0]):
            heur = conf[0].split("/")[1]
            print("Err heuristic {0} does not exist".format(heur))
            to_download += [heur]

    if len(to_download) > 1:
        print("Need to download:")
        print(" ". join(sorted(set(to_download))))

def check_conf_times(heur_conf):

    # Check that timing exists in a number or none
    for conf in heur_conf:
        if len(conf) != 2:
            print("Err missing time for conf: {0}".format(conf))

        else:
            # Test if time is None
            if conf[1] is not None:
                try:
                    # Test if number
                    float(conf[1])
                except ValueError as err:
                    print("Err conf {0}: ".format(conf), err)



def check_heuristic_requirements(heuristics):
    # For every file
    for heur in heuristics:

        # Get the contents of the file
        with open(heur, 'r') as f:
            d = f.read()

        # Make sure the string is not emtpy
        if d == "":
            print("Err heuristic {0} is empty".format(heur))
            continue

        # Check that proofs are suppressed (qbf)
        if "--res_out_proof false" in d:
            print("ERR heur {0}:  Suppressing resolution proof!".format(heur))
        if "--inst_out_proof false" in d:
            print("ERR heur {0}:  Suppressing instantiaton proof!".format(heur))
        if "--sat_out_model none" in d:
            print("ERR heur {0}:  Suppressing sat model!".format(heur))

        # Make sure timeout is removed from the heuristic
        if "--time_out_real" in d:
            print("ERR heur {0}: Timeout included!".format(heur))

        # Check that all heuristic has a clausifier set
        if "--clausifier res/vclausify_rel" not in d and "--clausifier res/eprover" not in d:
            print("ERR heur {0}: clausifier not set".format(heur))

        if "--clausifier_options" not in d and "fnt" not in heur:
            print("ERR heur {0}: clausifier options not set".format(heur))

        if "-t" not in d and "fnt" not in heur:
            print("ERR heur {0}: clausifier time is not set".format(heur))


def check_heuristic_validity(heuristics):

    for heur in heuristics:
        cmd_string, *_ = build_prover_cmd_external(test_problem, TIME, [heur, TIME], False, check_syntax=True)

        try:
            # Run prover on the test problem but just parse
            proc = subprocess.Popen(
                cmd_string,
                shell=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                universal_newlines=True)
            # Get the stderr
            out, err = proc.communicate(timeout=15)

        except subprocess.TimeoutExpired:
            # Process didn't satisfy the timeout of the communicate.
            err = "proc.communicate:TimeoutExpired"

        # Get exit code
        exit_code = proc.returncode

        # For some reason the error out can be "\n\n"
        # Therefore we convert this to the empty string
        if err == "\n\n":
            err = ""

        # Return the validity of the heurisristic and the error information
        if exit_code != 0 or err != "":
            print("Heuristic {0} is invalid!".format(heur))
            print(cmd_string)
            print(err)


def check_heuristic_execution(heuristics):

    ######################################################
    # Check that every heuristic in the path works!
    # This part does not reveal all bugs

    # EXTERNAL heuristic test
    for heur in heuristics:
        cmd_string, *_ = build_prover_cmd_external(test_problem, TIME, [heur, TIME])
        #print()
        #print(cmd_string)
        prover_proc = start_prover_process(cmd_string, TIME)
        _ = prover_proc.proc.communicate(timeout=TIME + 12)
        output_file = prover_proc.output_file

        # Check that error is empty
        with open(output_file + '_error') as f:
            #err = f.readlines()
            err = f.read()
            if err != "\n\n":
                print("ERR running heuristic: {0} ".format(heur), err)

        # print(prover_out)
        # Check that problem had the expcted status
        # Read the proof output
        with open(output_file, 'r') as f:
            prover_out = f.read()
        szs_status = get_prover_output_status(prover_out)

        if szs_status != "Theorem": # "Unsatisfiable":
            print("Err incorrect SZS status \"{0}\" for heuristic {1}".format(szs_status, heur))



"""
# INERNAL  heuristic test
    for heur in heuristics:

        # Build the schedule string
        sched_str = "[[{0};{1}]]".format(heur, TIME)

        cmd_string, *_ = build_prover_cmd_external(test_problem, TIME, [heur, TIME])
        prover_proc = start_prover_process(cmd_string, TIME)
        _, err = prover_proc.proc.communicate(timeout=TIME + 12)

        proc, output_file, *_ = start_prover_internal_schedule(test_problem, sched_str, TIME, heur)
        _, err = proc.communicate(timeout=TIME)

        # Check that error is empty
        if err != b"":
            print("ERR running INTERNAL heuristic: {0} ".format(heur), err)

        # print(prover_out)
        # Check that problem had the expcted status
        # Read the proof output
        with open(output_file, 'r') as f:
            prover_out = f.read()
        szs_status = get_prover_output_status(prover_out)

        if szs_status != "Satisfiable":
            print("Err incorrect SZS status \"{0}\" for heuristic {1}".format(szs_status, heur))
#"""

def check_admissible_embedding():

    adm_sched = []
    # Get the admissible schedules
    for name, sched in SCHEDULES.items():
        if isinstance(sched, dict):
            adm_sched += [(name, sched)]
            print("Got adm sched: ", name)

    # Get that each schedule has a model, default and cluster centers
    for name, sched in adm_sched:
        if "model" not in sched:
            print("{0} is missing the model path".format(name))
        if "default" not in sched:
            print("{0} is missing the default schedule".format(name))
        if "cluster_centers" not in sched:
            print("{0} is missing cluster_centers".format(name))


    # Check that the number of cluster centers and schedules are the same
    for name, sched in adm_sched:
        no_schedules = len([s for s in sched if isinstance(s, int)])
        if no_schedules != len(sched["cluster_centers"]):
            print("{0} has {1} schedules but {2} cluster centers (must be equal)".format(name, no_schedules, len(sched["cluster_centers"])))

    # Check that model and prediction works
    for name, sched in adm_sched:
        predict_local_schedule(sched, test_problem, 1, print_proof=False)


def main():

    # Get heuristic confs from all schedules
    heur_conf = extract_all_heuristic_configs()

    check_conf_exists(heur_conf)
    check_conf_times(heur_conf)

    # Get the heuristic files
    heuristics = [c[0] for c in heur_conf]
    heuristics = sorted(set(heuristics))

    check_heuristic_requirements(heuristics)
    check_heuristic_validity(heuristics)
    """
    #check_heuristic_execution(heuristics)

    check_admissible_embedding()
    #"""


if __name__ == "__main__":
    main() # Run tests



