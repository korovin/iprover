import argparse
import logging
import sys

import config
from helper import eprint


def get_parser():

    parser = argparse.ArgumentParser()

    # Positional arguments
    parser.add_argument("problem_path", help="Path to problem")
    parser.add_argument("wc_timeout", type=float, help="WC time limit for problem")

    # Optional Arguments
    parser.add_argument(
        "--no_cores", type=int, help="No cores available for proving (and embedding)", default=8
    )
    parser.add_argument(
        "--schedule", help="The schedule to run", default="default", choices=list(config.SCHEDULES.keys())
    )
    parser.add_argument(
        "--schedule_mode",
        help="How to run the heuristics on the prover",
        default="external",  # TODO add as type!
        choices=["external", "internal"],
    )
    parser.add_argument(
        "--heuristic_context",
        help="The context used to alter the heuristics",
        default="default",
        type=str,
        choices=list(config.CONTEXT_MODIFIERS.keys()),
    )

    parser.add_argument("--prover", default=None, help="Path to prover. Uses default in config if not set.")

    parser.add_argument(
        "--problem_version",
        default=None,
        choices=["fof", "cnf", "tff", "smt", "tx0"],
        help="Give the type of the problem (currently only used to distinguish tff/non-tff problems).",
    )
    parser.add_argument(
        "--ltb_problem",
        action="store_true",
        help="Set flag to treat the problem as an LTB problem (This is unrelated to the batchmode of --ltb)",
    )

    parser.add_argument(
        "--run_clausification_test",
        default=False,
        action="store_true",
        help="Set to run in internal schedule mode if the clausification test fails",
    )

    parser.add_argument("--pre_processor_heuristic", help="The heuristic to run as a pre-processor")
    parser.add_argument("--pre_processor_timeout", type=int, help="The timeout for the pre-processor")

    parser.add_argument(
        "--suppress_proof_model_out", help="Turn off proof reconstruction", action="store_true"
    )
    # LTB arguments
    parser.add_argument("--ltb", help="Set to run in LTB mode on BathcFile", action="store_true")
    parser.add_argument("--ltb_version", help="Which LTB version to run (tmp)", choices=["+1", "_1"])
    parser.add_argument("--ltb_output_dir", help="Output directory for LTB proofs")
    parser.add_argument(
        "--ltb_schedule_low_v4",
        help="The low time limit schedule for LTB (v4)",
        choices=list(config.SCHEDULES.keys()),
    )
    parser.add_argument(
        "--ltb_schedule_low_v5",
        help="The low time limit schedule for LTB (v5)",
        choices=list(config.SCHEDULES.keys()),
    )
    parser.add_argument(
        "--ltb_schedule_high",
        help="The high time limit schedule for LTB",
        choices=list(config.SCHEDULES.keys()),
    )

    # Set loglevels
    parser.add_argument(
        "-d",
        "--debug",
        help="Print debug logs",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        default=logging.WARNING,
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="Print info logs",
        action="store_const",
        dest="loglevel",
        const=logging.INFO,
    )

    # Return the parser
    return parser


def check_illegal_args(args):

    try:
        if args.wc_timeout < 0:
            eprint("ERROR: wc_timeout cannot be negative")
            raise ValueError

        if args.pre_processor_timeout is not None and args.pre_processor_timeout < 0:
            eprint("ERROR: pre_processor_timeout cannot be negative")
            raise ValueError
        if args.pre_processor_timeout is not None and args.pre_processor_timeout > args.wc_timeout:
            eprint("ERROR: pre_processor_timeout cannot be larger than wc_timeout")
            raise ValueError
        if args.pre_processor_timeout is not None and args.pre_processor_heuristic is None:
            eprint("ERROR: Need to specify preprocessor heuristic (no default)")
            raise ValueError

        # LTB options
        if args.pre_processor_heuristic and args.ltb:
            eprint("ERROR: preprocessing currently not supported for LTB")
            raise ValueError
        if args.ltb_schedule_low_v4 is not None and not args.ltb:
            eprint("ERROR: ltb_schedule_low_v4 only operational with ltb mode")
            raise ValueError
        if args.ltb_schedule_low_v5 is not None and not args.ltb:
            eprint("ERROR: ltb_schedule_low_v5 only operational with ltb mode")
            raise ValueError
        if args.ltb_schedule_high is not None and not args.ltb:
            eprint("ERROR: ltb_schedule_high only operational with ltb mode")
            raise ValueError

        # Check if the schedules are provided for LTB
        # DEPRECTAED
        """
        if args.ltb_schedule_low_v4 is None and args.ltb:
            eprint("ERROR: ltb_schedule_low_v4 is not provided!")
            raise ValueError()
        if args.ltb_schedule_low_v5 is None and args.ltb:
            eprint("ERROR: ltb_schedule_low_v5 is not provided!")
            raise ValueError()
        if args.ltb_schedule_high is None and args.ltb:
            eprint("ERROR: ltb_schedule_high is not provided!")
            raise ValueError()
        """

    except ValueError:
        eprint("EXIT ON ERROR")
        sys.exit(1)
